import time


def log_execution_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time

        with open('execution_times.log', 'a') as f:
            f.write(f'{func.__name__}: {execution_time}\n')

        return result
    return wrapper

# sample function to test time of execution


@log_execution_time
def test_function():
    time.sleep(1)

# sample function to test time of execution


@log_execution_time
def test_sum(a, b):
    return a + b


print(test_sum(100, 4))


test_function()
