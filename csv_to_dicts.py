import csv


def csv_to_dicts(filename):
    with open(filename, 'r') as f:
        # Use the csv module to read the file
        reader = csv.DictReader(f)

        # Convert the rows to dictionaries and return them as a list
        return list(reader)


# Test the function
filename = 'data.csv'
print(csv_to_dicts(filename))
