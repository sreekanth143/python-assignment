def anagrams_of_palindromes(words):
    def is_anagram_of_palindrome(word):
        # Create a dictionary to count the number of occurrences of each character
        char_counts = {}
        for char in word:
            char_counts[char] = char_counts.get(char, 0) + 1
        # Check if there are more than one odd count in the dictionary
        odd_counts = [count for count in char_counts.values() if count %
                      2 == 1]
        return len(odd_counts) <= 1
    return [word for word in words if is_anagram_of_palindrome(word)]


words = ['racecar', 'hello', 'level', 'carcare',
         'carecar', 'civic', 'lehol', 'vicic']
result = anagrams_of_palindromes(words)
print(result)
