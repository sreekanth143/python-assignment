def next_prime(n):
    def is_prime(num):
        if num < 2:
            return False
        for i in range(2, int(num ** 0.5) + 1):
            if num % i == 0:
                return False
        return True

    num = 2
    while n > 0:
        if is_prime(num):
            yield num
            n -= 1
        num += 1


# Test the generator function
print(list(next_prime(5)))  # [2, 3, 5, 7, 11]
print(list(next_prime(10)))  # [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
print(list(next_prime(30)))
# [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113]
