def sum_divisible_by_3_or_5(numbers):
    return sum(num for num in numbers if num % 3 == 0 or num % 5 == 0)


# Test the function
print(sum_divisible_by_3_or_5([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
print(sum_divisible_by_3_or_5([0, 15, 30, 45, 60, 75, 90, 105]))
