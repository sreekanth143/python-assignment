def get_integer_input():
    while True:
        try:
            return int(input("Enter an integer: "))
        except ValueError:
            print("ValueError exception handled, new input prompted")


# Test the function
x = get_integer_input()
print(x)  # prints the integer input entered by the user
