def sort_by_length(strings): return sorted(strings, key=lambda s: -len(s))


# Test the lambda function
strings = ["dog", "cat", "bird"]
print(sort_by_length(strings))  # ['bird', 'cat', 'dog']

strings = ["python", "java", "c++"]
print(sort_by_length(strings))  # ["python", "java", "c++"]


strings = ["sreekanth", "manoj", "vivek keshore"]
print(sort_by_length(strings))
