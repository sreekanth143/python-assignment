import os


def count_word_lengths(filename):
    # Create an empty dictionary to store the results
    results = {}

    # Open the file for reading
    with open(filename, 'r') as f:
        # Iterate over each line in the file
        for i, line in enumerate(f, 1):
            # Strip leading and trailing whitespace from the line
            line = line.strip()

            # Split the line into words
            words = line.split()

            # Create a dictionary to store the counts for this line
            counts = {}

            # Iterate over each word in the line
            for word in words:
                # Get the length of the word
                length = len(word)

                # If the length is not already in the counts dictionary, add it with a count of 1
                if length not in counts:
                    counts[length] = 1
                # Otherwise, increment the count for this length
                else:
                    counts[length] += 1

            # Add the counts dictionary to the results dictionary
            results[i] = counts

    return results


# Test the function with a sample file
filename = 'sample.txt'
print(count_word_lengths(filename))
